// Package bab provides an interface to the DP Battery Aggregator Board.
package bab

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
)

const getAllRecCount = 10
const startChar = '$'
const endChar = '\r'

var ErrFormat = errors.New("Invalid format")
var ErrChannel = errors.New("Invalid channel")
var ErrNoResponse = errors.New("No response")

type intParser struct {
	err error
}

func (p *intParser) Parse(s string) int {
	if p.err != nil {
		return 0
	}
	var val int
	val, p.err = strconv.Atoi(s)

	return val
}

// State contains the information for a single battery
type State struct {
	// Battery channel number, 0-9
	Channel int `json:"channel"`
	// Battery voltage in mV
	Voltage int `json:"voltage"`
	// Battery current in mA, >0 incoming, <0 outgoing
	Amperage int `json:"amperage"`
	// Battery capacity in %
	Capacity int `json:"capacity"`
}

func (s State) String() string {
	return fmt.Sprintf("channel=%d voltage=%d amperage=%d capacity=%d",
		s.Channel,
		s.Voltage,
		s.Amperage,
		s.Capacity)
}

func (s *State) DecodeText(text []byte) error {
	if text[0] != '$' {
		return ErrFormat
	}

	p := &intParser{}
	if i := bytes.Index(text, []byte("*")); i > 0 {
		fields := bytes.Split(text[1:i], []byte(","))
		if len(fields) < 4 {
			return fmt.Errorf("Invalid record; %q: %w", text, ErrFormat)
		}
		s.Channel = p.Parse(string(fields[0]))
		s.Voltage = p.Parse(string(fields[1]))
		s.Amperage = p.Parse(string(fields[2]))
		s.Capacity = p.Parse(string(fields[3]))
		return p.err
	}

	return ErrFormat
}

// Summary contains the average for all available batteries
type Summary struct {
	// Number of batteries
	Count int `json:"count"`
	// Average voltage in mV
	Voltage int `json:"voltage"`
	// Average current in mA
	Amperage int `json:"amperage"`
	// Average capacity in %
	Capacity int `json:"capacity"`
}

func (s Summary) String() string {
	return fmt.Sprintf("count=%d voltage=%d amperage=%d capacity=%d",
		s.Count,
		s.Voltage,
		s.Amperage,
		s.Capacity)
}

func (s *Summary) DecodeText(text []byte) error {
	if text[0] != '$' {
		return ErrFormat
	}

	p := &intParser{}
	if i := bytes.Index(text, []byte("*")); i > 0 {
		fields := bytes.Split(text[1:i], []byte(","))
		if len(fields) < 4 {
			return fmt.Errorf("Invalid record; %q: %w", text, ErrFormat)
		}
		s.Count = p.Parse(string(fields[0]))
		s.Voltage = p.Parse(string(fields[1]))
		s.Amperage = p.Parse(string(fields[2]))
		s.Capacity = p.Parse(string(fields[3]))
		return p.err
	}

	return ErrFormat
}

// Read and return the next BAB output record.
func readRec(rdr io.Reader) ([]byte, error) {
	char := make([]byte, 1)
	rec := make([]byte, 0, 64)

	for {
		n, err := rdr.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for START: %w", err)
		}
		if char[0] == startChar {
			rec = append(rec, char[0])
			break
		}
	}

	for {
		n, err := rdr.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for END: %w", err)
		}
		if char[0] == endChar {
			break
		}
		rec = append(rec, char[0])
	}

	return rec, nil
}

func skipRec(rdr io.Reader) error {
	_, err := readRec(rdr)
	return err
}

// Device is a BAB board connected to a serial port
type Device struct {
	rw io.ReadWriter
}

func New(rw io.ReadWriter) *Device {
	return &Device{rw: rw}
}

// GetAll returns the State for each battery.
func (d *Device) GetAll() ([]State, error) {
	states := make([]State, 0, getAllRecCount)
	fmt.Fprint(d.rw, "$GETALL\r")
	// Skip the command echo
	if err := skipRec(d.rw); err != nil {
		return states, err
	}
	if err := skipRec(d.rw); err != nil {
		return states, err
	}

	for i := 0; i < getAllRecCount; i++ {
		rec := State{}
		b, err := readRec(d.rw)
		if err != nil {
			return states, err
		}
		if err := rec.DecodeText(b); err != nil {
			return states, err
		}
		states = append(states, rec)
	}

	return states, nil
}

// GetCh returns the state of the battery on channel n
func (d *Device) GetCh(n int) (State, error) {
	var state State
	if n < 0 || n > 9 {
		return state, ErrChannel
	}

	fmt.Fprintf(d.rw, "$GETCH%d\r", n)
	// Skip the command echo
	if err := skipRec(d.rw); err != nil {
		return state, err
	}
	if err := skipRec(d.rw); err != nil {
		return state, err
	}

	b, err := readRec(d.rw)
	if err != nil {
		return state, err
	}
	err = state.DecodeText(b)

	return state, err
}

// GetAve returns a Summary for all batteries
func (d *Device) GetAve() (Summary, error) {
	var summary Summary

	fmt.Fprint(d.rw, "$GETAVE\r")
	// Skip the command echo
	if err := skipRec(d.rw); err != nil {
		return summary, err
	}
	if err := skipRec(d.rw); err != nil {
		return summary, err
	}

	b, err := readRec(d.rw)
	if err != nil {
		return summary, err
	}
	err = summary.DecodeText(b)

	return summary, err
}
