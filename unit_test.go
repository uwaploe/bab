package bab

import (
	"bytes"
	"strings"
	"testing"
)

const EOL = "\n\r"

var getallResp = []string{
	"$GETALL",
	"$GETALL",
	"$0,16128,0,89*",
	"$1,16284,0,89*",
	"$2,16288,0,89*",
	"$3,16258,0,89*",
	"$4,0,0,0*",
	"$5,0,0,0*",
	"$6,0,0,0*",
	"$7,0,0,0*",
	"$8,0,0,0*",
	"$9,0,0,0*\n\r",
}

func TestScan(t *testing.T) {
	input := strings.NewReader(strings.Join(getallResp, EOL))

	count := 0
	for {
		_, err := readRec(input)
		if err != nil {
			break
		}
		count++
	}

	if count != len(getallResp) {
		t.Errorf("Wrong record count; expected %d, got %d",
			len(getallResp), count)
	}
}

func TestGetall(t *testing.T) {
	input := bytes.NewBuffer([]byte(strings.Join(getallResp, EOL)))
	dev := New(input)
	_, err := dev.GetAll()
	if err != nil {
		t.Fatal(err)
	}
}

func TestUnmarshalState(t *testing.T) {
	in := "$0,16128,0,89*"
	out := "channel=0 voltage=16128 amperage=0 capacity=89"

	var state State
	err := state.DecodeText([]byte(in))
	if err != nil {
		t.Fatal(err)
	}

	if state.String() != out {
		t.Errorf("Output error; expected %q, got %q", out, state.String())
	}
}
